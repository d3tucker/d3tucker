# Copyright 1999-2019 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

# ebuild generated by hackport 0.6.1.9999

CABAL_FEATURES="lib profile haddock hoogle hscolour"
inherit haskell-cabal

DESCRIPTION="Parse files conforming to the xdg desktop entry spec"
HOMEPAGE="http://hackage.haskell.org/package/xdg-desktop-entry"
SRC_URI="mirror://hackage/packages/archive/${PN}/${PV}/${P}.tar.gz"

LICENSE=""	# FIXME: license unknown to cabal. Please pick it manually.
SLOT="0/${PV}"
KEYWORDS="~amd64 ~x86"
IUSE=""

RDEPEND="dev-haskell/configfile:=[profile?]
	dev-haskell/either:=[profile?]
	dev-haskell/multimap:=[profile?]
	dev-haskell/safe:=[profile?]
	>=dev-lang/ghc-8.6.1:=
"
DEPEND="${RDEPEND}
	>=dev-haskell/cabal-2.4.0.1
"
