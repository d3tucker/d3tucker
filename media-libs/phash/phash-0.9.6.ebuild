EAPI=6
inherit eutils

DESCRIPTION="Perceptual hashing of image, video, and audio files"
HOMEPAGE="http://www.phash.org/"
SRC_URI="http://www.phash.org/releases/pHash-0.9.6.tar.gz"
LICENSE="GPL-3"
SLOT="0"

S="${WORKDIR}/pHash-${PV}"

KEYWORDS="amd64 x86"

IUSE="audio debug fftw image java video jpeg png openmp static-libs threads"

DEPEND="image? ( media-libs/cimg )
		video? ( media-libs/cimg virtual/ffmpeg )
		audio? ( media-libs/libsndfile media-libs/libsamplerate media-sound/mpg123 )
		java? ( virtual/jdk )
		fftw? ( sci-libs/fftw )"

RDEPEND="$DEPEND"

src_configure() {
	econf \
		$(use_enable audio audio-hash ) \
		$(use_enable image image-hash ) \
		$(use_enable video video-hash ) \
		$(use_enable debug ) \
		$(use_enable static-libs static ) \
		$(use_enable java ) \
		$(use_enable openmp ) \
		$(use_with fftw )
		$(use_with jpeg libjpeg ) \
		$(use_with png libpng )
}
