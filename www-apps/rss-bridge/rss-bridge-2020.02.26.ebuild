# Copyright 2018 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit webapp

DESCRIPTION=""
HOMEPAGE=""
SRC_URI="https://github.com/RSS-Bridge/${PN}/archive/$(ver_rs 1-2 '-').tar.gz -> ${P}.tar.gz"

LICENSE="public-domain"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

DEPEND=""
RDEPEND="${DEPEND}"
BDEPEND=""

need_httpd_cgi

S="${WORKDIR}/${PN}"

src_install() {
	webapp_src_preinst

	insinto "${MY_HTDOCSDIR}"
	doins -r *

	webapp_src_install
}
